module.exports = function(grunt) {
    var replace = require("replace");
    var settings = {
        paths: {
            publicFolder: ".",
            mainScssFile: "demo",
            assetsFolder: ".",
        }
    };
    grunt.file.copy(
        "scss/_avant.scss",
        "scss/demo.scss"
    );

    replace({
        regex: "@import \"../../",
        replacement: "@import \"../bower_components/",
        paths: ["scss/demo.scss"],
        recursive: false
    });

    require("flexible-gruntfile")(grunt, settings);
    grunt.registerTask("checkproject", "Checking project...", function() {
        return;
    });
};
